#ifndef AST_HEADER
#define AST_HEADER

#include <stdbool.h>
#include "lexer.h"

typedef union AST AST;
typedef struct ListNode ListNode;
typedef struct AbbrevToken AbbrevToken;

typedef struct Identifier Identifier;
typedef struct List List;

typedef union Declaration   Declaration;
typedef struct FunctionDecl FunctionDecl;
typedef struct VarDecl      VarDecl;

typedef struct Expression       Expreession;
typedef struct BinaryExpr       BinaryExpr;
typedef struct UnaryMinusExpr   UnaryMinusExpr;
typedef struct LiteralExpr      LiteralExpr;
typedef struct IdExpr           IdExpr;
typedef struct FunctionCallExpr FunctionCallExpr;

typedef struct Statement  Statement;
typedef struct AssignStat AssignStat;
typedef struct ReturnStat ReturnStat;
typedef struct ExprStat   ExprStat;

typedef enum DeclType DeclType;
typedef enum StatType StatType;
typedef enum ExprType ExprType;

typedef enum ListType ListType;

#include "visitor.h"

enum DeclType {
	VARIABLE_DECLARATION = 10,
	VARIABLE_DECLARATION_LIST, // unused
	FUNCTION_DECLARATION,
};

enum StatType {
	VAR_DECL_STATEMENT = 20,
	ASSIGN_STATEMENT,
	RETURN_STATEMENT,
	EXPRESSION_STATEMENT,
};

enum ExprType {
	BINARY_EXPRESSION = 30,
	UNARY_MINUS_EXPRESSION,
	LITERAL_EXPRESSION,
	IDENTIFIER_EXPRESSION,
	FUNCTION_CALL_EXPRESSION,
};

// not necessary, but, if needed, you may identify what is
// the type of the list by checking this enum
enum ListType {
	LIST_OF_EXPRESSIONS = 40,
	LIST_OF_DECLARATIONS,
	LIST_OF_STATEMENTS,
};

struct ListNode {
	AST *ast;
	ListNode *next;
};

union AST {

	union Declaration {
		DeclType type;

		struct FunctionDecl {
			int padding; // useless space to align union fields
			Type return_type;
			AST *id;
			AST *param_decl_list;
			AST *stat_block;
			AbbrevToken *token;
		} function;

		struct VarDecl {
			int padding; // useless space to align union fields
			Type type;
			AST *id;
			AST *expr;
			AbbrevToken *token;
			AbbrevToken *assign_token;
		} variable;

	} decl;


	struct List {
		ListType type;
		int num_items;
		ListNode *first, *last;
	} list;


	struct Expression {
		ExprType type;

		union {
			struct BinaryExpr {
				char operation; // '+', '-', '*', '/' '%'
				AST *left_expr;
				AST *right_expr;
				AbbrevToken *token;
			} binary_expr; 

			struct UnaryMinusExpr {
				AST *expr;
				AbbrevToken *token;
			} unary_minus;

			struct LiteralExpr {
				Type type;
				union {
					double float_value;
					long int_value;
				};
				AbbrevToken *token;
			} literal;

			struct IdExpr {
				AST *id;
				AbbrevToken *token;
			} id;

			struct FunctionCallExpr {
				AST *id;
				AST *expr_list;
				AbbrevToken *token;
			} function_call;
		};

	} expr; // base type


	struct Statement {
		StatType type;

		union {
			List var_decl_list; // TODO: implement this

			//struct VarDecl var_decl; //use decl.variable

			struct AssignStat {
				AST *id;
				AST *expr;
				AbbrevToken *token;
			} assign;

			struct ReturnStat {
				AST *expr;
				AbbrevToken *token;
			} ret;

			struct ExprStat {
				AST *expr; // TODO: remove this indirection
			} expr;
		};

	} stat; // base type


	struct Identifier {
		char *string; // name of the identifier
		int hash;     // hash of the name
		int length;   // length of the name

		AST *declaration; // points to the the AST node where it was declared
		DeclType decl_type; // VARIABLE_DECLARATION or FUNCTION_DECLARATION

		Type type; // type of the identifier
		bool is_global;

		// what the variable currently stores or is referred as	
		// store the result of expression-visitor functions here
		// when an assignment is made to the variable
		ExprResult store;
	} id;

};

struct AbbrevToken {
	char *filename, *line_start;
	int line, column;
};
	

void init_memory ();
AST *new_literal (TokenType type, void *value, Token *token);
AST *new_identifier (Token *token, bool at_global_scope);
AST *new_id_expr (AST *id, Token *token);
AST *new_binary_expr (AST *leftExpr, char op, AST *right_expr, Token *token);
AST *new_unary_minus (AST *expr, Token *token);
AST *new_function_call (AST *id, AST *expr_list, Token *token);
AST *new_list (AST *expr);
AST *add_to_list (AST *expr_list, AST *expr);
AST *new_var_decl_list (AST *ast);
AST *new_expr_stat (AST *expr);
AST *new_return_stat (AST *expr, Token *token);
AST *new_assign_stat (AST *id, AST *expr, Token *token);
AST *new_var_decl (Type type, AST *id, AST *expr, Token *token, Token *assign_token);
//AST *new_param_decl (Type type, AST *id, Token *token);
AST *new_function_decl (Type type, AST *id, AST *param_decl_list, AST *stat_block, Token *token);
AST *new_file (AST *decl);
AST *add_to_file (AST *file, AST *decl);
char *strlinea (const AbbrevToken *token);

#endif // AST_HEADER
