#ifndef ANALYZER_HEADER
#define ANALYZER_HEADER

#include "ast.h"

typedef struct ExprResult ExprResult;
typedef enum ExprResultType ExprResultType;

enum ExprResultType {
	SSA_REGISTER,
	INTEGER_CONSTANT,
	FLOAT_CONSTANT,
};

struct ExprResult {
	union {
		long int_value;
		double float_value;
		long ssa_register;
	};
	ExprResultType type; // enum
};

void analyze_file (AST *root);
//void analyze_global_var_decl (AST *ast);
ExprResult analyze_stat (AST *stat);
ExprResult analyze_return_stat (AST *assign);
void analyze_assign_stat (AST *assign);
void analyze_var_decl (AST *ast);
void analyze_function_decl (AST *ast);
ExprResult analyze_stat_block (AST *stat_block, AST *params, TypeType return_type);
ExprResult analyze_expr (AST *expr);
ExprResult analyze_add (AST *expr);
ExprResult analyze_sub (AST *expr);
ExprResult analyze_mul (AST *expr);
ExprResult analyze_div (AST *expr);
ExprResult analyze_mod (AST *expr);
ExprResult analyze_id (AST *ast);
ExprResult analyze_literal (AST *ast);
ExprResult analyze_unary_minus (AST *ast);
ExprResult analyze_function_call (AST *ast);

#endif // ANALYZER_HEADER

