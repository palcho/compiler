#ifndef VISITOR_HEADER
#define VISITOR_HEADER

#include "lexer.h"

typedef struct ExprResult ExprResult;

struct ExprResult {
	union {
		// only one value may be stored here at a time
		long int_value;     // if type == TYPE_INT
		double float_value; // if type == TYPE_FLOAT
		long ssa_register;  // if type == SSA_REGISTER
	};
	Type type; // defines which member of the union is being used
};

#include "ast.h"


void visit_file (AST *root);
//void visit_global_var_decl (AST *ast);
ExprResult visit_stat (AST *stat);
ExprResult visit_return_stat (AST *assign);
void visit_assign_stat (AST *assign);
void visit_var_decl (AST *ast);
void visit_function_decl (AST *ast);
ExprResult visit_stat_block (AST *stat_block, AST *params, Type return_type);
ExprResult visit_expr (AST *expr);
ExprResult visit_add (AST *expr);
ExprResult visit_sub (AST *expr);
ExprResult visit_mul (AST *expr);
ExprResult visit_div (AST *expr);
ExprResult visit_mod (AST *expr);
ExprResult visit_id (AST *ast);
ExprResult visit_literal (AST *ast);
ExprResult visit_unary_minus (AST *ast);
ExprResult visit_function_call (AST *ast);

#endif // VISITOR_HEADER
