#ifndef LEXER_HEADER
#define LEXER_HEADER

// structs prototypes
typedef struct Token Token;
typedef enum TokenType TokenType;
typedef enum Type Type;

enum TokenType {
	// '\0' '=' '+' '-' '/' '*' '{' '}' '(' ')' ';' ',' '!' '>' '<' '&' '|' '^'
	UNKNOWN = 0x80,   // unrecognized character
	TYPE,             // the type of the type is specified at enum Type
	IDENTIFIER,       // variable or function name
	INTEGER,          // integer number literal
	FLOAT,            // floating point number literal
	STRING,           // a string delimited by " "
	RETURN,           // return

	ARROW,            // ->
	ADD_ASSIGN,       // +=
	SUB_ASSIGN,       // -=
	MUL_ASSIGN,       // *=
	DIV_ASSIGN,       // /=
	MOD_ASSIGN,       // %=
	AND_ASSIGN,       // &=
	OR_ASSIGN,        // |=
	XOR_ASSIGN,       // ^=
	EQUAL,            // ==
	NOT_EQUAL,        // !=
	GREATER_OR_EQUAL, // >=
	LESSER_OR_EQUAL,  // <=
	AND,              // &&
	OR,               // ||
};

// used either as a type of identifier or of expression result at the visitor
enum Type {
	NO_TYPE,   // identifier has no type // unused
	TYPE_VOID,      // only functions may be void
	TYPE_INT,
	TYPE_FLOAT,
	TYPE_STRING,    // placeholder for char * (not used in lexer, but later)
	SSA_REGISTER,
};


struct Token {
	char *filename;    // name of the file read by the lexer
	char *file_buffer; // pointer to start of the file
	char *buffpos;     // pointer to next character to be read
	char *start;       // pointer to the start of the token just read
	char *line_start;  // pointer to start of line where buffpos is

	int length;    // length of the token
	int line;      // line of the token
	int column;    // column in current line where the token starts
	int line_span; // number of lines the token spans

	union {
		long int_value;     // value of the integer literal token
		double float_value; // value of the floating point literal token
		long id_hash;       // hash of the identifier's string
		Type type_type; // enum
	};

	TokenType type; // enum
}; 


// function prototypes
int next_token (Token *token);
char *strtoken (const Token *token);
char *strline (const Token *token);
int jenkins_hash (char *string, int length);

#endif // LEXER_HEADER
